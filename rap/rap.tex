\documentclass{article}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{fullpage}

\newcommand\transp[1]{{}^t #1}


\title{PROG : Projet C++ "Inversion de matrices"}
\author{C. Ferry \and B. Thomas}
\date{}


\newcommand{\trans}{\ensuremath{\mathrm{T}}}
\DeclareMathOperator{\com}{com}
\DeclareMathOperator{\cof}{Cof}


\begin{document}
% Les parties ne seront pas équilibrées, il faudra voir si on fait une partie 
% entière pour l'évaluation de la méthode de Gauss-Jordan, de nouveaux noms 
% pour les sous-parties sont aussi à trouver.

\maketitle
\tableofcontents


\section*{Introduction}
\addcontentsline{toc}{section}{Introduction}
Les matrices sont largement utilisées en informatique - notamment pour les 
simulations physiques - car elles introduisent un formalisme adapté aux 
problèmes de calculs faisant intervenir de nombreux paramètres.
Toutefois leur utilisation reste assez lourde et nécessitent vite un important 
temps de calcul.
C'est en particulier le cas du problème de l'inversion de matrices, que nous 
avons étudié ici.
Ce projet consiste à écrire un programme inversant une matrice donnée $M$. 
L'objectif est avant tout d'exploiter finement des fonctionnalités 
propres au langage C++ ainsi que de la programmation orientée objet.

Ce rapport présente deux façons d'inverser une matrice : la première basée sur 
une formule mathématique simple, est facile d'implémentation mais est en 
complexité temporelle exponentielle, et ne peut donc pas être utilisée pour des 
matrices de taille importante. La seconde méthode est de complexité temporelle 
polynomiale (cubique), et permet donc d'inverser des matrices beaucoup plus 
grandes.



\section{Inversion par le calcul de la comatrice.}
\subsection{Présentation de la méthode.}
Cette première méthode se base sur le calcul de la comatrice : on sait en effet 
qu'étant donné une matrice $M$, son inverse $M^{-1}$ peut être calculée par la 
formule suivante :
\[  M^{-1} = \frac{\transp{\com(M)}}{\det(M)}  \]
où $\det(M)$ est le déterminant de $M$, $\transp{}$ est l'opérateur transposée 
et où $\com(M)$ est la comatrice de $M$ définie comme suit :
\[  com(M) = \begin{Bmatrix}
        \Delta_{1, 1} & \Delta_{1, 2} & \cdots & \Delta_{1, n} \\
        \Delta_{2, 1} & \Delta_{2, 2} & \cdots & \Delta_{2, n} \\
        \vdots           & \vdots           & \ddots & \vdots            \\
        \Delta_{n, 1} & \Delta_{n, 2} & \cdots & \Delta_{n, n} \\
        \end{Bmatrix}  \]
avec :
\[  \Delta_{i, j} = (-1)^{i + j}  \cdot \cof_{i, j}  \]
où $\cof_{i, j}$ est le déterminant de la matrice $M$ privée de sa $i$-ième 
ligne et de sa $j$-ième colonne.

Calculer la matrice inverse revient alors à calculer des déterminants de 
matrices. Pour cela, on développe simplement le déterminant par sa première 
colonne :
\[ \begin{vmatrix}
      a_{1, 1} & a_{1, 2} & \cdots & a_{1, n} \\
      a_{2, 1} & a_{2, 2} & \cdots & a_{2, n} \\
      \vdots    & \vdots    & \ddots & \vdots    \\
      a_{n, 1} & a_{n, 2} & \cdots & a_{n, n}  \\
   \end{vmatrix} =
   a_{1, 1} \cdot
   \begin{vmatrix}
      a_{2, 2} & \cdots & a_{2, n} \\
      \vdots    & \ddots & \vdots    \\
      a_{n, 2} & \cdots & a_{n, n}  \\
    \end{vmatrix}
    + \ldots +
    (-1)^{n-1}a_{n, 1} \cdot
    \begin{vmatrix}
      a_{1, 2}    & \cdots & a_{1, n} \\
      \vdots       & \ddots & \vdots    \\
      a_{n-1, 2} & \cdots & a_{n-1, n} \\
    \end{vmatrix}
\]

On peut alors calculer le déterminant - et donc l'inverse - de toute matrice 
$M$ en appliquant récursivement cette formule.

\subsection{Architecture du code}
La première étape de ce projet a été d'implémenter une classe \texttt{Matrix}.
Ces classes, contenant des vecteurs à deux dimensions, sont munies des
méthodes suivantes :
\begin{itemize}
\item \texttt{Matrix(unsigned n, unsigned p)} le constructeur prenant en 
argument la taille de la matrice désirée.
\item \texttt{unsigned get\_size\_i(void)} et \texttt{unsigned 
get\_size\_j(void)} permettant d'accéder à ses dimensions.
\item \texttt{void set(unsigned i, unsigned j, scalar\_t x)} permettant 
d'affecter une valeur dans la matrice.
\item \texttt{scalar\_t get(unsigned i, unsigned j)} permettant d'accéder à une 
valeur de la matrice.
\end{itemize}

Ces méthodes ont permis de construire les opérateurs de somme, de soustraction 
et de multiplication par un scalaire ou une matrice. Nous avons ainsi défini 
les opérateurs \texttt{operator+}, \texttt{operator-}, \texttt{operator*} 
(cet opérateur a été surchargé pour permettre le produit d'une matrice par un 
scalaire).

D'autres fonctions sur les matrices ont ensuite été ajoutées dans un fichier à 
part (\texttt{matrix\_misc.cc}). Elles implémentent l'inversion présentée plus 
haut : 
\begin{itemize}
\item \texttt{Matrix submatrix(Matrix M, unsigned int i, unsigned int j)} 
permet d'obtenir la sous matrice de \texttt{M} après retrait de la ligne 
\texttt{i} et de la colonne \texttt{j}
\item \texttt{Matrix::scalar\_t determinant(Matrix M)} calcule le déterminant 
de manière récursive en utilisant la fonction précédente.
\item \texttt{Matrix transpose(Matrix M)} calcule la transposée de la matrice 
\texttt{M}.
\item \texttt{Matrix inverse(Matrix M)} utilise toute les fonctions précédentes 
pour calculer l'inverse de \texttt{M} comme décrit plus haut.
\end{itemize}

On trouve de plus dans \texttt{matrix\_misc} trois fonctions permettant de 
générer des matrices : la première pour la matrice identité, la deuxième pour 
la matrice de Hilbert, réputée difficile à inverser en calcul flottant, et la 
dernière génère une matrice à coefficients aléatoires. Ces fonctions ont été 
utilisées pour tester le programme.

\subsection{Défauts de la méthode}
Cette méthode pour inverser une matrice présente un défaut majeur : pour 
calculer le déterminant d'une matrice de taille $n$, il faut calculer $n$ 
déterminants de taille $n-1$, ce qui conduit à une complexité temporelle en 
$O(n!)$ et donc non polynomiale. De ce fait, l'inversion d'une matrice de 
taille importante est impensable avec cette méthode. Un autre problème est 
que les sous-matrices extraites pour calculer les déterminants contiennent une 
copie des coefficients, ce qui génère une complexité spatiale importante.

Lorsque la complexité spatiale pourrait être allgée par des manipulations 
d'indice astucieuses (en utilisant des sous-matrices implicites), la complexité 
temporelle est intrinsèque à la méthode utilisée.

\section{Méthode de Gauss-Jordan}

\subsection{Présentation de la méthode}
La méthode de Gauss-Jordan consiste à utiliser des \textit{opérations 
élémentaires} sur les lignes \textit{ou bien} sur les colonnes des matrices 
pour déterminer son inverse.
Il existe trois telles opérations élémentaires :
\begin{itemize}
\item (1) $L_i \leftarrow L_i + x \times L_j$
\item (2) $L_i \leftarrow t \times L_i$
\item (3) $L_i, L_j \leftarrow L_j, L_i$
\end{itemize}
où $L_i$ représente la $i$-ième ligne de la matrice.

En réalité, effectuer une des deux premières opérations sur une matrice revient 
à la multiplier par une matrice de transposition $T_{i, j}(x)$, définie comme 
étant la matrice identité à laquelle on a remplacé le coefficient en position 
$(i, j)$ par $x$.

À partir de ces opérations et en appliquant l'algorithme du \emph{pivot de 
Gauss} sur une matrice $M$, on trouve en fait une suite de matrices de 
transposition $T_1, \ldots T_m$ telle que $M \times T_1 \times \ldots \times 
T_m = I_n$ d'où par associativité du produit matriciel $M^{-1} = T_1 \times 
\ldots \times \times T_m$.

Ainsi, on applique le pivot de Gauss sur $M$ pour obtenir $I_n$, et en 
appliquant les mêmes opérations en partant de $I_n$, on obtient $M^{-1}$.

\subsection{Architecture du code}

L'implémentation de cet algorithme se fait par la classe \texttt{MatGJ} qui 
contient trois matrices :
\begin{itemize}
\item \texttt{Ori} est la matrice originale qui ne sera pas modifiée.
\item \texttt{Aux} est une copie de la matrice originale qui sera transformée 
en $I_n$.
\item \texttt{Inv} est initialisée comme $I_n$ mais est modifiée pour contenir 
l'inverse de la matrice initiale.
\end {itemize}

Les trois opérations sur les lignes sont implémentées dans cette classe de 
manière à agir à la fois sur \texttt{Aux} et sur \texttt{Inv} comme décrit dans 
la partie précédente. Ces fonctions sont des membres privés de la classe 
\texttt{MatGJ}, car elles n'ont pas vocation à être appelées par l'utilisateur 
directement.
Une fonction \texttt{get\_max} a aussi été implémentée de 
manière à choisir le pivot le plus approprié afin d'éviter autant que possible 
les divisions par des flottants proches de zéro en choisissant le coefficient 
de plus grande valeur absolue.

À partir de ces quelques fonctions, une fonction effectuant le pivot de Gauss a 
été implémentée et est appelée une seule fois lors de la construction de 
l'objet.

\subsection{Evaluation}
% Evaluation de notre implémentation, comparaison avec la méthode naïve...

\paragraph{Espace mémoire occupé} Nous avons implémenté, dans les constructeurs 
de la classe \texttt{Matrix}, un compteur de taille en tant que membre 
\texttt{static}. Celui-ci compte l'espace mémoire occupé par les coefficients 
de toutes les matrices instanciées.
Des accesseurs comme \texttt{Matrix::getCurrentSize(void)}, 
\texttt{Matrix::getMaximumSize(void)}, permettent d'obtenir les valeurs de ces 
compteurs. Ces accesseurs sont des membres \texttt{static} de la classe 
\texttt{Matrix}, de sorte qu'elles sont utilisables sans objet instancié.

La taille maximale atteinte par le programme lors d'une inversion est en figure 
\ref{mem}. Cette taille ne dépend que de la taille des matrices en jeu.
À l'aide d'une régression logarithmique, on détermine que l'espace mémoire 
occupé par notre programme est en $\mathcal{O}(n^2)$.

L'espace occupé est proportionnel au nombre de matrices en jeu : dans la 
classe \texttt{Matrix\_GJ}, on trouve trois matrices de la taille de la matrice 
à inverser. Une matrice étant de taille $n^2 \times 
$\texttt{sizeof(scalar\_t)}, l'inversion d'une matrice a un coût mémoire 
proportionnel à $n^2$.

Nous n'avons pas compté la taille de l'objet \texttt{Matrix} que l'on crée dans 
le constructeur ni la taille des \texttt{Vector} utilisés. Seul l'espace occupé 
par les coefficients est compté.

\begin{figure}
\centering
\includegraphics[width=0.45\columnwidth]{size}
\includegraphics[width=0.45\columnwidth]{size_log}

\caption{\label{mem} L'espace mémoire occupé par le programme, dans le cadre 
d'une inversion par la méthode de Gauss-Jordan. À droite, les mêmes données, en 
échelle logarithmique.}
\end{figure}

\paragraph{Précision de l'inversion}
Nous avons fait tourner notre programme sur des matrices remplies aléatoirement
de valeurs dans l'intervalle $[1;100]$, suivant une loi uniforme.
Les mesures de précision obtenues sont en figure \ref{dev}.

\begin{figure}
\centering
\includegraphics[width=0.45\columnwidth]{precision_left}
\includegraphics[width=0.45\columnwidth]{precision_right}

\caption{\label{dev} La déviation dans le cadre d'inversion de matrices 
aléatoires suivant une loi uniforme, entre le produit $MM^{-1}$ à gauche, 
$M^{-1}M$ à droite, et la matrice identité. La mesure choisie est la norme 1 
canonique des matrices.}
\end{figure}

La déviation du produit $MM^{-1}$ (ou $M^{-1}M$) avec la matrice identité est 
de l'ordre de $10^{-12}$ à $10^{-14}$ pour des matrices de taille inférieure à 
100. L'écart-type des mesures est assez significatif, il est parfois de presque 
un ordre de grandeur : cela indique une forte dépendance entre la validité de 
l'inverse trouvé et les coefficients de la matrice d'origine.

Les matrices de Hilbert, de la forme $(a_{i,j})_{i,j\in 
\mathbf{N}_n} = (\frac{1}{i+j-1})$ engendrent des dérives dûes à la précision de 
l'unité de calcul en virgule flottante des processeurs. Nous avons testé notre 
programme sur ces matrices; le résultat est en figure \ref{hilb}.

\begin{figure}
\centering
\includegraphics[width=0.45\columnwidth]{hilbert_left_dev}
\includegraphics[width=0.45\columnwidth]{hilbert_right_dev}

\caption{\label{hilb} La déviation provoquée par les matrices de Hilbert, entre 
le produit $MM^{-1}$ et l'identité, et $M^{-1}M$ et l'identité. La mesure 
utilisée ici est la norme 1 canonique de la matrice.}
\end{figure}

On constate que les matrices de Hilbert ne sont pas correctement inversées par 
notre programme : les erreurs de précision liées à l'usage du 
type \texttt{double} provoquent une erreur sur l'inverse qui fait que la 
déviation $ H_n H_n^{-1} - \mathrm{Id}_n$ est de norme supérieure à 100.

\section*{Conclusion}
\addcontentsline{toc}{section}{Conclusion}

Nous avons implémenté deux méthodes d'inversion de matrices. Il s'agissait 
essentiellement de faire un usage fin du C++ et de la programmation orientée 
objet, ainsi les méthodes proposées sont mathématiquement simples et leur 
implémentation ne présente pas d'optimisation. 

Nous avons modifié les constructeurs de la classe \texttt{Matrix}; nous aurions 
pu intégrer les compteurs dans l'opérateur \texttt{operator new}. Cependant, 
cela n'aurait eu d'effet que lors d'une allocation utilisant le mot-clé 
\texttt{new} (et donc utilisant des pointeurs).

Nous aurions également pu implémenter une classe de scalaire permettant de 
réduire, voire annuler, l'erreur commise dans le calcul de l'inverse, en 
particulier dans le cas des matrices de Hilbert.

\end{document}
