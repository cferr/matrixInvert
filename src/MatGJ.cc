#include "MatGJ.hh"

/** Constructor for Gauss-Jordan calculator.
 * Constructing such an object triggers the inversion.
 * @param Mat The matrix to be inverted
 */
MatGJ::MatGJ(Matrix Mat) : Ori(Mat.get_size_i(), Mat.get_size_i()),
  Aux(Mat.get_size_i(), Mat.get_size_i()),
  Inv(Mat.get_size_i(), Mat.get_size_i()) {

  assert(Mat.get_size_i() == Mat.get_size_j());

  n = Mat.get_size_i();

  for (unsigned i = 0; i < n; i++) {
    for (unsigned j = 0; j < n; j++) {
      Ori.set(i, j, Mat.get(i, j));
      Aux.set(i, j, Mat.get(i, j));
      if (i==j) {
        Inv.set(i, j, 1.0);
      }
    }
  }
  this->Gauss_Jordan();
}

/** Get the size of the inverted matrix */
unsigned MatGJ::get_size() {
  return n;
}

/** Performs line_i <- (line_i + t*line_j).
 * @param i The destination line
 * @param j The addend line
 * @param t The multiplicand scalar
 */
void MatGJ::line_op(unsigned i, Matrix::scalar_t t, unsigned j) {
  /* line_i <- (line_i + t*line_j), assuming the k first ellements are set to
  0. */
  assert(0 <= i && i < n);
  assert(0 <= j && j < n);

  for (unsigned l = 0; l < n; l++) {
    Aux.set(i, l, (Aux.get(i, l) + t*Aux.get(j, l)));
    Inv.set(i, l, (Inv.get(i, l) + t*Inv.get(j, l)));
  }
}

/** Multiplies the line i by t.
 * @param i The target line
 * @param t The multiplicand scalar.
 */
void MatGJ::line_mul(unsigned i, Matrix::scalar_t t) {
  assert(0 <= i && i < n);

  for (unsigned l = 0; l < n; l++) {
    Aux.set(i, l, t*Aux.get(i, l));
    Inv.set(i, l, t*Inv.get(i, l));
  }
}

/** Interverts lines i and j.
 * @param i The intervertand
 * @param j The intervertee
 */
void MatGJ::line_exchange(unsigned i, unsigned j) {
  assert(0 <= i && i < n);
  assert(0 <= j && j < n);

  Matrix::scalar_t aux;

  for (unsigned l = 0; l < n; l++) {
    aux = Aux.get(i, l);
    Aux.set(i, l, Aux.get(j, l));
    Aux.set(j, l, aux);
    aux = Inv.get(i, l);
    Inv.set(i, l, Inv.get(j, l));
    Inv.set(j, l, aux);
  }
}

/** Get argmax_k {abs(M(k,j))}.
 * @param i The line from which the search will start.
 * @param j The column from which the maximum should be retrieved.
 */
unsigned MatGJ::get_max(unsigned i, unsigned j) {
  unsigned im = i;
  Matrix::scalar_t max = abs(Aux.get(i, j));
  for (unsigned l = i + 1; l < n; l++) {
    if (max < abs(Aux.get(l, j))) {
      im = l;
      max = abs(Aux.get(l, j));
    }
  }
  return im;
}

/** Compute the inverse of M */
void MatGJ::Gauss_Jordan() {
  for (unsigned k = 0; k < n; k++) {
    line_exchange(k, get_max(k, k));
    line_mul(k, (1/Aux.get(k, k)));
    for (unsigned i = 0; i < n; i++) {
      if (i != k) {
        line_op(i, -Aux.get(i, k), k);
      }
    }
  }
}
