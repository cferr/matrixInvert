#include <iostream>
#include <iomanip>

#include <string>
#include <cmath>
#include <cassert>
#include <cstdlib>

#include <vector>

#include "matrix.hh"

using namespace std;

class MatGJ {
private:
  unsigned n;
  void line_op(unsigned i, Matrix::scalar_t t, unsigned j);
  void line_mul(unsigned i, Matrix::scalar_t t);
  void line_exchange(unsigned i, unsigned j);
  unsigned get_max(unsigned i, unsigned j);

public:
  MatGJ(Matrix Mat);
  Matrix Ori, Aux, Inv;
  unsigned get_size();
  void Gauss_Jordan();
};