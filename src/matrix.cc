#include "matrix.hh"

using namespace std;


/******************************************************************************/
/*               ALLOCATION AND MEMORY MANAGEMENT SECTION                     */
/******************************************************************************/

unsigned int Matrix::instantaneousSize = 0;
unsigned int Matrix::instantaneousAllocs = 0;
unsigned int Matrix::maximumSize = 0;
unsigned int Matrix::maximumAllocs = 0;
unsigned int Matrix::cumulatedAllocs = 0;
bool Matrix::block = true;

/** Allocates a matrix. Please note that calling this function directly
 * with the construction Matrix::operator new() doesn't result in the
 * default constructor being called afterwards. */
void* Matrix::operator new (size_t size) {
  // We may allocate a large pool then pick matrices in that
  // pool. As a matrix object doesn't contain any array, this
  // would result in a small, if any, time gain.
  void *p = malloc(sizeof(Matrix));
  if(!p) {
    std::bad_alloc exn;
    throw exn;
  }
  return p;
}

/** Deletes a given matrix pointer. */
void Matrix::operator delete(void* p) {
  // Simple as that.
  free(p);
}

/** Default constructor for the class Matrix.
 * @param n The size of this matrix's first dimension.
 * @param n The size of this matrix's second dimension.
 */
Matrix::Matrix(unsigned n, unsigned p) {
  size_i = n;
  size_j = p;
  assert(1 <= size_i);
  assert(1 <= size_j);

  contents = vector<vector<scalar_t>/**/>(size_i);
  for (unsigned i = 0; i < size_i; i++)
    contents.at(i) = vector<scalar_t>(size_j, 0.0); // Initialization to 0.0

  /* Statistics */
  if(!Matrix::block) {
    instantaneousSize += (n * p * sizeof(scalar_t)); // don't count overhead
    maximumSize = max(maximumSize, instantaneousSize);
    instantaneousAllocs++;
    maximumAllocs = max(maximumAllocs, instantaneousAllocs);
    cumulatedAllocs++;
  }
}

/** Copy constructor.
 * @param m The matrix to be copied.
 */
Matrix::Matrix(const Matrix& m) {
  if(!Matrix::block) {
    instantaneousSize += (m.get_size_i() *
                          m.get_size_j() * sizeof(scalar_t));
    maximumSize = max(maximumSize, instantaneousSize);
    instantaneousAllocs++;
    maximumAllocs = max(maximumAllocs, instantaneousAllocs);
    cumulatedAllocs++;
  }
  this->size_i = m.get_size_i();
  this->size_j = m.get_size_j();
  this->contents = m.get_contents(); // this is a COPY !
}

/** Copy affectation operator.
 * @param m The matrix to be copied.
 */
Matrix& Matrix::operator=(const Matrix& m) {
  if(!Matrix::block) {
    instantaneousSize += (m.get_size_i() *
                          m.get_size_j() * sizeof(scalar_t));
    maximumSize = max(maximumSize, instantaneousSize);
    instantaneousAllocs++;
    maximumAllocs = max(maximumAllocs, instantaneousAllocs);
    cumulatedAllocs++;
  }
  this->size_i = m.get_size_i();
  this->size_j = m.get_size_j();
  this->contents = m.get_contents(); // this is also a copy !
  return *this;
}

/** Destructor. */
Matrix::~Matrix() {
  if(!Matrix::block) {
    instantaneousSize -= this->get_size_i() * this->get_size_j()
                         * sizeof(scalar_t);
    instantaneousAllocs--;
  }
}

/** Output matrix class statistics */
void Matrix::statistics() {
  cout << "----- Program Statistics -----" << endl;
  cout << "Current cumulated matrix size is " << Matrix::getCurrentSize()
       << " bytes" << endl;
  cout << "Maximum size reached has been " << Matrix::getMaximumSize()
       << " bytes" << endl;
  cout << "Currently " << Matrix::getCurrentAllocs() << " matrices "
       << "are allocated" << endl;
  cout << "At most " << Matrix::getMaximumAllocs() << " matrices "
       << "have been allocated simultaneously" << endl;
  cout << "A total of " << Matrix::getMaximumAllocs() << " allocations "
       << "have been done so far" << endl;

}

/******************************************************************************/
/*                        MATRIX FUNCTIONS SECTION                            */
/******************************************************************************/


/****************************** ACCESSORS *************************************/

/** Get the current matrix's number of lines. */
unsigned Matrix::get_size_i() const {
  return size_i;
}

/** Get the current matrix's number of columns. */
unsigned Matrix::get_size_j() const {
  return size_j;
}

/** Set the coefficient at index (i,j).
 * @param i The line which coefficient should be set.
 * @param j The column which coefficient should be set.
 */
void Matrix::set(unsigned i, unsigned j, scalar_t x) {
  assert(0 <= i && i < size_i);
  assert(0 <= j && j < size_j);
  contents.at(i).at(j) = x;
}

/** Get the coefficient at index (i,j).
 * @param i The line which coefficient should be retrieved.
 * @param j The column which coefficient should be retrieved.
 */
Matrix::scalar_t Matrix::get(unsigned i, unsigned j) const {
  assert(0 <= i && i < size_i);
  assert(0 <= j && j < size_j);
  return contents.at(i).at(j);
}

/** Pretty-print the matrix. */
void Matrix::print() const {
  for (unsigned i = 0; i < size_i; i++) {
    for (unsigned j = 0; j < size_j; j++) {
      cout << setprecision(2) << setw(8) << contents.at(i).at(j);
    }
    cout << endl;
  }
  cout << "___________________________" << endl;
}

/******************************* OPERATORS ************************************/

/** Calculate the sum of M1 and M2.
 * @param M1 The augend matrix
 * @param M2 The addend matrix
 */
Matrix operator+(const Matrix& M1, const Matrix& M2) {
  assert(M1.get_size_i() == M2.get_size_i());
  assert(M1.get_size_j() == M2.get_size_j());
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();
  Matrix M(size_i, size_j);

  for (unsigned i = 0; i < size_i; i++) {
    for (unsigned j = 0; j < size_j; j++) {
      Matrix::scalar_t x =  M1.get(i, j) + M2.get(i, j);
      M.set(i, j, x);
    }
  }

  return M;
}

/** Calculate the difference of M1 and M2.
 * @param M1 The minuend matrix
 * @param M2 The subtrahend matrix
 */
Matrix operator-(const Matrix& M1, const Matrix& M2) {
  assert(M1.get_size_i() == M2.get_size_i());
  assert(M1.get_size_j() == M2.get_size_j());
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();
  Matrix M(size_i, size_j);

  for (unsigned i = 0; i < size_i; i++) {
    for (unsigned j = 0; j < size_j; j++) {
      Matrix::scalar_t x =  M1.get(i, j) - M2.get(i, j);
      M.set(i, j, x);
    }
  }

  return M;
}

/** Calculate the scalar product of M1 and a.
 * @param M1 The matrix
 * @param a The scalar
 */
Matrix operator*(Matrix::scalar_t a, const Matrix& M1) {
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();
  Matrix M(size_i, size_j);

  for (unsigned i = 0; i < size_i; i++) {
    for (unsigned j = 0; j < size_j; j++) {
      Matrix::scalar_t x =  a * M1.get(i, j);
      M.set(i, j, x);
    }
  }

  return M;
}

/** Calculate the product of M1 and M2.
 * @param M1 The multiplicand matrix
 * @param M2 The multiplier matrix
 */
Matrix operator*(const Matrix& M1, const Matrix& M2) {
  assert(M1.get_size_j() == M2.get_size_i());

  unsigned size_i = M1.get_size_i();
  unsigned size_j = M2.get_size_j();
  unsigned size_k = M1.get_size_j();
  Matrix M(size_i, size_j);

  for (unsigned i = 0; i < size_i; i++) {
    for (unsigned j = 0; j < size_j; j++) {
      Matrix::scalar_t x = 0;
      for(unsigned k = 0; k < size_k; k++) {
        x += M1.get(i, k) * M2.get(k, j);
      }
      M.set(i, j, x);
    }
  }

  return M;

}
