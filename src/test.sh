#!/bin/bash

# Compile

make clean
make

# Run tests for the first pass -> showing the results

./inverse -n 10 -stats

read

# Run tests for a Hilbert matrix

./inverse -n 10 -hilbert -stats

read

# Statistics

# Parse statistics and build a graph with R

echo -ne "" > ./stats_w

for j in {1..100}; do
    ./inverse -n $j -stats >> ./stats_w
done

cat ./stats_w | grep "Maximum size" | cut -c 31- | rev | cut -c 6- | rev > ./size
echo -e "MatrixSize\tMemory" > ./size_r
(awk '{printf "%d\t%s\n", NR, $0}' < ./size) >> ./size_r
R --no-save < size.r 
xdg-open size.pdf &> /dev/null &

echo "Next step : Test inconsistencies for Hilbert matrices"
read

echo -ne "" > ./stats_s

echo -e "MatrixSize\tLeftPrec\tRightPrec" > ./prec_r
for i in {1..4}; do
    for j in {3..70}; do
        ./inverse -n $j -hilbert -stats > ./stats_m
        sed -n '0~8p' ./stats_m > ./prec_pre_r
        sed -s 's/ /\t/g' ./prec_pre_r > ./pre_pre_prec_r
        (awk '{printf "%d\t%s\n", '$j', $0}' < ./pre_pre_prec_r) >> ./prec_r
    done
done

R --no-save < prec.r
xdg-open precision.pdf &> /dev/null &

# awk '{printf "%d\t%s\n", NR, $0}' < ./maxsize$i to append line numbers


echo "Next step : Test inconsistencies for random matrices"
read

echo -ne "" > ./stats_s

echo -e "MatrixSize\tLeftPrec\tRightPrec" > ./prec_r
for i in {1..4}; do
    for j in {3..70}; do
        ./inverse -n $j -stats > ./stats_kl
        sed -n '0~8p' ./stats_kl > ./wprec_pre_r
        sed -s 's/ /\t/g' ./wprec_pre_r > ./wpre_pre_prec_r
        (awk '{printf "%d\t%s\n", '$j', $0}' < ./wpre_pre_prec_r) >> ./prec_r
    done
done

R --no-save < prec.r
xdg-open precision.pdf &> /dev/null &
