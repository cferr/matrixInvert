#pragma once

#include <iostream>
#include <iomanip>
#include <string>
#include <cassert>
#include <cmath>
#include <vector>

#include <cstdlib>

using namespace std;

class Matrix {
public:
  typedef double scalar_t;

private:

  static unsigned int instantaneousSize;
  static unsigned int instantaneousAllocs;
  static unsigned int maximumSize;
  static unsigned int maximumAllocs;
  static unsigned int cumulatedAllocs;
  //static long int cumulatedSize = 0; // update these fields
  static bool block;
  // keep trace of our program's size... (chained list to avoid max iter?)

  unsigned size_i, size_j;
  vector<vector<scalar_t>/**/> contents;

public:
  Matrix(unsigned n, unsigned p);
  unsigned get_size_i(void) const;
  unsigned get_size_j(void) const;
  void set(unsigned i, unsigned j, scalar_t x);
  scalar_t get(unsigned i, unsigned j) const;

  vector<vector<scalar_t> > get_contents() const {
    return contents;
  }

  void print() const;

  // allocation and deletion
  static void* operator new(size_t size);
  static void operator delete(void* p);

  // copy, destructors
  Matrix(const Matrix& m);
  Matrix& operator=(const Matrix& m);
  ~Matrix();

  // accessors
  static unsigned int getCurrentSize() {
    return instantaneousSize;
  }
  static unsigned int getMaximumSize() {
    return maximumSize;
  }
  static unsigned int getCurrentAllocs() {
    return instantaneousAllocs;
  }
  static unsigned int getMaximumAllocs() {
    return maximumAllocs;
  }
  static unsigned int getCumulatedAllocs() {
    return cumulatedAllocs;
  }
  static void statistics();

  static void startCounting() {
    block = false;
  }

};

/*****************************************************/

Matrix operator+(const Matrix& M1, const Matrix& M2);

Matrix operator-(const Matrix& M1, const Matrix& M2);

Matrix operator*(Matrix::scalar_t a, const Matrix& M1);

Matrix operator*(const Matrix& M1, const Matrix& M2);