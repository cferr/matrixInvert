data <- read.table("./prec_r", header=T)
pdf("precision.pdf")
boxplot(data$LeftPrec~data$MatrixSize, data=data, xlab = "Matrix Size", ylab = "Precision", log="y")
#Sys.sleep(10)
