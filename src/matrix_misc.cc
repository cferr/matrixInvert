#include "matrix_misc.hh"

/** Identity matrix of size n.
 * @param n The size of the identity matrix to get
 */
Matrix Id(unsigned n) {
  Matrix M(n, n);
  for (unsigned i = 0; i < n; i++) {
    M.set(i, i, 1.0);
  }
  return M;
}

/** Hibert matrix, that is known to cause troubles with floating-point
 * inverters.
 * @param n The size of the requested Hilbert matrix
 */
Matrix hilbert(unsigned n) {
  Matrix M(n, n);
  for (unsigned i = 0; i < n; i++) {
    for (unsigned j = 0; j < n; j++) {
      M.set(i, j, 1.0/(i + j + 1));
    }
  }
  return M;
}

/** Create a square matrix filled with random scalars between min and max,
 * following a uniform repartition.
 * @param n The size of the matrix
 * @param min The minimum possible value in the matrix
 * @param max The maximum possible value in the matrix
 */
Matrix random(unsigned n, Matrix::scalar_t min, Matrix::scalar_t max) {
  Matrix M(n, n);
  for (unsigned i = 0; i < n; i++) {
    for (unsigned j = 0; j < n; j++) {
      Matrix::scalar_t aux = (Matrix::scalar_t) rand()/RAND_MAX;
      M.set(i, j, aux*min + (1.0-aux)*max);
    }
  }
  return M;
}

/** Calculate the 1-norm of a given matrix M1 (the sum of its absolute values).
 * @param M1 The matrix to evaluate
 */
Matrix::scalar_t norm(const Matrix& M1) {
  assert(M1.get_size_i() > 0 && M1.get_size_j() > 0);
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();

  Matrix::scalar_t res = abs(M1.get(0,0));
  for (unsigned i = 0; i < size_i; i++) {
    for (unsigned j = 0; j < size_j; j++) {
      res = max(abs(M1.get(i,j)), res);
    }
  }
  return res;
}

/** Get a sub-matrix from matrix M1, where line a and column b have been
 * removed.
 * @param M1 The source matrix
 * @param a The line to remove
 * @param b The column to remove
 */
Matrix submatrix(const Matrix& M1, unsigned a, unsigned b) {
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();
  assert (0 <= a && a < size_i);
  assert (0 <= b && b < size_j);
  assert(size_i >= 2);
  assert(size_j >= 2);

  Matrix M(size_i - 1, size_j - 1);

  for(unsigned i = 0; i < size_i - 1; i++) {
    for(unsigned j = 0; j < size_j - 1; j++) {
      M.set(i,j, M1.get((i < a)?i:i+1, (j < b)?j:j+1));
    }
  }

  return M;
}

/** Get (-1)^i. */
static int toggle(unsigned i) {		//Note it is static!
  return (i%2)?-1:1;
}

/** Compute the determinant of a square matrix M1 by developing its
 * first column.
 * @param M1 The matrix of which the determinant should be computed.
 */
Matrix::scalar_t determinant(const Matrix& M1) {
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();
  assert(size_i == size_j);
  assert(size_i > 0);

  if (size_i == 1) return M1.get(0, 0);

  Matrix::scalar_t sum = 0;

  for(unsigned j = 0; j < size_j; j++) {
    Matrix Mbis = submatrix(M1, 0, j);
    sum += toggle(j)*M1.get(0, j)*determinant(Mbis);
  }

  return sum;
}

/** Get the transposed matrix of M1.
 * @param M1 The transposer matrix.
 */
Matrix transpose(const Matrix& M1) {
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();
  Matrix Mres(size_j, size_i);
  for (unsigned i = 0; i < size_i; i++) {
    for (unsigned j = 0; j < size_j; j++) {
      Mres.set(j, i, M1.get(i, j));
    }
  }
  return Mres;
}


/** Get the inverse of the matrix M1 using the cofactors matrix : 
 * M1^(-1) = 1 / det(M) * transpose(cofactors(M1))
 * @param M1 The matrix to be inverted
 */
Matrix inverse(const Matrix& M1) {
  unsigned size_i = M1.get_size_i();
  unsigned size_j = M1.get_size_j();
  assert(size_i == size_j);

  Matrix::scalar_t det = determinant(M1);
  if (det == 0) {
    cerr << "Cannot invert a matrix with null determinant" << endl;
    M1.print();
    exit(1);
  }

  unsigned size = size_i;
  Matrix cofactors(size, size);
  Matrix M2 = transpose(M1);	// Do not forget it!

  for (unsigned i = 0; i < size; i++) {
    for (unsigned j = 0; j < size; j++) {
      Matrix M = submatrix(M2, i, j);

      Matrix::scalar_t x = determinant(M);
      Matrix::scalar_t y =  toggle(i+j) * (x / det);
      cofactors.set(i, j, y);
    }
  }

  return cofactors;
}
