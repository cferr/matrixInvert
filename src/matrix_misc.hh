#pragma once

#include <cstdlib>
#include <ctime>


#include "matrix.hh"


Matrix Id(unsigned n);
Matrix hilbert(unsigned n);
Matrix random(unsigned n, Matrix::scalar_t min, Matrix::scalar_t max);

Matrix::scalar_t norm(const Matrix& M1);

Matrix submatrix(const Matrix& M1, unsigned a, unsigned b);

Matrix::scalar_t determinant(const Matrix& M1);

Matrix transpose(const Matrix& M1);

Matrix inverse(const Matrix& M1);
