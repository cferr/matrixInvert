#include <iostream>
#include "matrix.hh"
#include "matrix_misc.hh"
#include "MatGJ.hh"

#include <cstdlib>


using namespace std;

/*! Demonstration program for our Matrix class. */
int main(int argc, char* argv[]) {

  /* Initialisation of the seed for rand() */
  srand (time(NULL));

  unsigned int n = 10;

  bool hilb = false;
  bool stats = false;
  bool printMatrices = false;
  // getopt() does this better, yet it's a bit heavy for such a use
  for(int i = 0; i < argc; i++) {
    /* why is i signed ? just because the C++ standard requires
       argc to be a signed int, then comparison with unsigned makes
       little sense */
    std::string arg = std::string(argv[i]);
    if(arg == "-n" && i < argc - 1)
      n = atoi(argv[i+1]);
    if(arg == "-stats") {
      stats = true;
      Matrix::startCounting();
    }
    if (arg == "-hilbert") {
      hilb = true;
    }
    if(arg == "-print")
      printMatrices = true;
  }

  Matrix M(n, n);
  if (hilb) {
    M = hilbert(n);
  } else {
    M = random(n, -10, 10);
  }

  MatGJ A(M);
  if(printMatrices) {
    A.Ori.print();
    A.Inv.print();
  }

  // Show a little bit of statistics
  if(stats) {
    Matrix::statistics();
    cout << "The precision of the inversion is:" << endl;
    cout << norm((A.Ori * A.Inv) - Id(n)) << " " <<
         norm((A.Inv * A.Ori) - Id(n)) << endl;
  }

}
