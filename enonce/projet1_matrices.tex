\documentclass{../../../LaTeX/tdsimple}

\usepackage[utf8]{inputenc}%
\usepackage[francais]{babel}%
\usepackage{listings}%
\usepackage{url}%

\usepackage{wasysym}%
\usepackage{ifthen}%

\DeclareMathOperator{\determinant}{det}

\begin{document}

\title{ENS Rennes\\
  L3 Informatique, parcours R \& I\\
  Programmation avancée, 2\ieme\ semestre 2015--2016\\
  ---\\
  Projet 1: Programmation scientifique\\
  Inversion de matrices}

\author{%
  Luc Bougé%
}

\date{11 février 2016%
  \thanks{%
    D'après un énoncé de Yann Salmon, 2012--2013.\protect\\
    \protect\lstinline[language={}]%
    $Id: projet1_matrices.tex 1910 2016-02-11 08:53:51Z bouge $%
  } }

% svn propset svn:keywords Id *.tex

\maketitle

\section*{Introduction}

\subsection*{Présentation du projet}

Le but de ce projet est de créer une classe de matrices et d'effectuer
sur elles les opérations classiques de l'algèbre linéaire, en
particulier le calcul du déterminant et l'inversion.  

On définit une classe générale pour les matrices de taille
$n \times p$. Le constructeur de base sera donc de la forme %
\|Matrix(unsigned n, unsigned p)|. Le type des éléments est paramétrable. Les
matrices sont des tableaux à deux dimensions. On va donc utiliser dans
un premier temps un conteneur \|vector| qui contiendra des conteneurs
\|vector| de coefficients.

Pour vous permettre d'avancer rapidement dans les premières étapes, un
petit squelette est disponible sur le site Piazza du cours.

Les modalités de rendu seront fixées lors du lancement du projet.

  \begin{remarque}
    La documentation sur les \emph{manipulateurs} d'affichage %
    (\|setprecision(2)|, \|fixed|, \|setw(8)|, etc.) se trouve à l'URL
    \begin{center}
      \url{http://www.cplusplus.com/reference/iostream/manipulators/}.
    \end{center}
  \end{remarque}

  \subsection*{Déroulement et évaluation}

  Le sujet est composé d'une partie obligatoire commune et d'un choix
  de parties optionnelles ouvertes, graduées de une à trois
  étoiles. Il est demandé de choisir l'une des parties optionnelles,
  pas plus. Par contre, un grand soin est attendu dans le travail de
  conception et de validation.

  Vous avez le droit de vous inspirer et même de reprendre les codes
  que vous avez écrits dans les séances de TP précédentes. Vous êtes
  encouragés à utiliser vos notes de cours, tout document et tout
  livre à votre convenance, et de consulter sur Internet la
  documentation C++ ou tout site qui vous paraîtrait utile.

  \section{Partie obligatoire: opérations sur les matrices}

  \begin{remarque}
    Temps estimé pour cette partie: 4~heures.
  \end{remarque}
    
  \begin{attention}
    L'objectif est ici la \emph{robustesse} du codage, pas la
    performance. Toute la phase de codage doit être traitée de manière
    \emph{défensive} en utilisant toutes des assertions à chaque fois
    que c'est possible. Utilisez largement les variables
    intermédiaires pour être sûrs de vos calculs. Ne cherchez surtout
    pas à optimiser, mais assurez-vous de la correction de vos
    fonctions pas à pas.
  \end{attention}

  \begin{remarque}
    On n'accède à la matrice qu'à travers les méthodes de
    la classe. Normalement, les méthodes proposées dans le squelette
    devraient être suffisante. 

    Cependant, s'il vous manque des accès, ajoutez des
    méthodes. Gardez cependant à l'esprit que le nombre de méthodes
    doit être le plus petit possible pour assurer la robustesse du
    code. Utilisez plutôt des fonctions externes autour de la classe.
  \end{remarque}

\subsection{Opérations arithmétiques}

  \begin{question}
    Définissez une fonction externe \|operator +| qui calcule la somme
    de deux matrices, et une fonction externe \|operator -| qui
    calcule la différence.
\begin{lstlisting}
Matrix operator+(const Matrix& M1, const Matrix& M2);
Matrix operator-(const Matrix& M1, const Matrix& M2);
\end{lstlisting}
    \begin{attention}
      Cette méthode doit vérifier que les tailles des matrices sont
      compatibles par des assertions.
    \end{attention}
  \end{question}

  \begin{question}
    Définissez une fonction externe \|operator *| qui calcule le
    produit d'une matrice par un scalaire de type
    \|Matrix::scalar_t|.
\begin{lstlisting}
Matrix operator*(Matrix::scalar_t a, const Matrix& M1);
\end{lstlisting}
    \begin{attention}
      Toute cette partie du code doit utiliser le type scalaire
      générique \|Matrix::scalar_t|. Le type \|double| ne doit
      apparaître nulle part dans votre code.
    \end{attention}
  \end{question}

  \begin{question}
    Écrivez une fonction externe \|operator *| qui calcule le produit
    de deux matrices.
    \begin{attention}
      Cette méthode doit vérifier que les tailles des matrices sont
      compatibles par des assertions.
    \end{attention}
\begin{lstlisting}
Matrix operator*(const Matrix& M1, const Matrix& M2);
\end{lstlisting}
  \end{question}

  \begin{question}
    Définissez une fonction externe qui construit la transposée d'une
    matrice.
\begin{lstlisting}
Matrix transpose(const Matrix& M1);
\end{lstlisting}
  \end{question}

  \begin{question}
    Définissez une fonction externe qui construit la matrice carrée
    unité de taille donnée.
\begin{lstlisting}
Matrix Id(unsigned n);
\end{lstlisting}
  \end{question}

  \begin{question}
    Définissez une fonction externe qui calcule la norme (maximum des
    valeurs absolues des termes) d'une matrice.
\begin{lstlisting}
Matrix::scalar_t norm(const Matrix& M1);
\end{lstlisting}
    \begin{attention}
      Les normes des matrices que vous allez obtenir sont très
      petites. Il faudra donc les imprimer en notation scientifique:
      \|cout << scientific << norm(M) << endl;|
    \end{attention}
  \end{question}

\subsection{Inverse par la méthode de Cramer}

L'objectif est de calculer le déterminant d'une matrice par une
méthode \emph{brute force}: le développement selon une colonne. Le
premier pas est de produire la sous-matrice associée à un élément de
la matrice. Voir les détails mathématiques par exemple sur la page
suivante:
\begin{center}
  \url{https://fr.wikipedia.org/wiki/R%C3%A8gle_de_Cramer}
\end{center}

\begin{question}
  Définissez une fonction qui construit la matrice de taille $(n, p)$
  obtenue en enlevant la ligne $a$ et la colonne $b$ d'une matrice
  $M$. Cette méthode est utilisée seulement pour le calcul du
  déterminant. Elle ne doit pas être exportée. Elle doit donc être
  qualifiée \|static| (au sens de~C).
\begin{lstlisting}
static Matrix submatrix(const Matrix& M1, unsigned a, unsigned b);
\end{lstlisting}
  \begin{attention}
    Il faut verifier que que $0 \le a < n$ et $0 \le b < p$ et que la
    taille de la matrice initiale est assez grande. 
  \end{attention}
\end{question}

\begin{question}
  Utilisez cette fonction pour calculer par récursion sur la taille
  $n$ d'une matrice carrée son déterminant par la formule de Cramer.
\begin{lstlisting}
Matrix::scalar_t determinant(const Matrix& M1);
\end{lstlisting}
  \begin{attention}
    Il faut vérifier que la matrice est bien carrée.  Notez que vous
    aurez à calculer des termes de la forme $(-1)^k$. Il sera
    intéressant de créer une fonction auxiliaire pour cela.
\begin{lstlisting}
int toggle(unsigned k)
\end{lstlisting}
  \end{attention}
\end{question}

\begin{question}
  Utilisez cette même approche par sous-matrices pour calculer
  l'inverse d'une matrice $M$.
\begin{lstlisting}
Matrix inverse(const Matrix& M1);
\end{lstlisting}
  \begin{attention}
    Il faut bien sûr vérifier que $M$ est une matrice carrée et que
    son déterminant est non-nul.
  \end{attention}
\end{question}

\subsection{Fonctions de vérification}

\newcommand{\Id}{{\text{Id}}}

\begin{question}
  Écrivez une fonction qui prend une matrice $M$, calcule son inverse
  $M'$ et calcule la norme de $M \times M' - \Id$ et $M' \times M - \Id$
  où $\Id$ est la matrice identité. Cette norme est la mesure de la
  qualité de votre calcul.
\end{question}

\begin{question}
  Appliquez ce test sur un choix représentatif de matrices que vous
  proposerez. Utilisez en particulier la matrice de Hilbert:
  \begin{center}
    \url{https://fr.wikipedia.org/wiki/Matrice_de_Hilbert}
  \end{center}
Étudiez en particulier ce qui se passe quand la taille de la matrice
augmente. Essayez de justifier la perte de précision par l'étude de la
stabilité numérique de votre implémentation. 
\end{question}

\section{Parties au choix: optimisations}

  \begin{remarque}
    Temps estimé pour cette partie: 4~heures.
  \end{remarque}

\newcounter{starcounter}

\newcommand{\starlevel}[1]{%
\setcounter{starcounter}{0}%
\whiledo {\value{starcounter} < #1}{%
$\star$\ %
\stepcounter{starcounter}%
}%
}

\subsection{Vérifications algébriques \protect\starlevel{1}}

Vérifiez que les lois mathématiques sur les déterminants et les
inverses sont bien vérifiées. Par exemple:
\[
\begin{array}{l}
\determinant(A \times B) = \determinant(A) \times \determinant(B) \\
\determinant(B \times A) = \determinant(A \times B) \\
 (A \times B)^{-1} = {A ^{-1}} \times {B ^{-1}}\\
\end{array}
\]
Vous pouvez bien sûr en ajouter d'autres selon vos connaissances en algèbre.

\subsection{Utilisation des complexes \protect\starlevel{1}}

Créez une classe \|Complex| des nombres complexes, définissez sur ces
complexes toutes les opérations nécessaires et remplacez dans la
classe \|Matrix| le type \|double| par le type \|Complex|. Si vous
avez bien programmé, tout doit bien se passer!
\begin{lstlisting}
typedef Complex scalar_t;
\end{lstlisting}
Pourquoi pas définir et manipuler des matrices de matrices, par exemple?

\subsection{Utilisation des fractions \protect\starlevel{2}}

Plus difficile, définissez une classe de nombres fractionnaires $p/q$
et inversez des matrices de tels nombres. Il faut bien sûr
réduire les fractions autant que possible. 
\begin{attention}
  Vous serez conduits à manipuler des entiers très grands. Utilisez le
  type \|long|.
\end{attention}

\subsection{Mesure de temps \protect\starlevel{1}}

La bibliothèque \|<ctime>| contient la fonction \|clock_t clock(void)|
qui renvoie le nombre de battements de l'horloge depuis le démarrage
de votre programme.

  Mesurez le temps d'exécution de portions de votre programme en les
  encadrant par des appels à \|clock()|.
\begin{lstlisting}
clock_t before = clock(); 
// Code to time
clock_t after = clock(); 
cout << "Number of ticks: " << after - before << endl;
\end{lstlisting}
  Pour convertir une valeur rendue par la fonction \|clock()| en
  secondes, vous devez la diviser par la constante \|CLOCKS_PER_SEC|.

  \begin{attention}
    Il faut faire très attention à ce que vous mesurez! Prenez
    plusieurs mesures successives d'un code, éliminez systématiquement
    les premières et indiquez \emph{toujours} l'écart-type en plus de
    la moyenne. Voici un schéma-type pour le calcul du temps
    moyen. À vous de traiter l'écart-type, ô futurs chercheurs!
\begin{lstlisting}
  const unsigned NB_PURGE = 10;
  const unsigned NB_MEASURE = 10 * NB_PURGE;
  Matrix MM = inverse(M); 
  
  for (unsigned i = 0; i < NB_PURGE; i++) {     // Purge
    MM = inverse(M);
  }
  clock_t before = clock();
  for (unsigned i = 0; i < NB_MEASURE; i++) {   // Actual measure
    MM = inverse(M);
  }
  clock_t after = clock();
  double seconds = double(after - before) / CLOCKS_PER_SEC;
  cout << "CPU time in seconds: " << seconds / NB_MEASURE << endl;
\end{lstlisting}
  \end{attention}

  Étudiez par exemple le temps nécessaire pour calculer l'inverse
  d'une matrice en fonction de sa taille. 
  \begin{attention}
    Lisez attentivement la documentation de la fonction \|clock| pour
    vérifier de quel temps il s'agit et comment il est décompté.
    \begin{center}
      \url{http://www.cplusplus.com/reference/ctime/clock/}
    \end{center}
    De manière générale, votre processus de mesure \emph{ne devrait pas
    durer moins de quelques secondes} pour être fiable par rapport à
    toutes les opérations annexes du système. Utiliser la fonction Unix
    \sh|time| pour le vérifier.
\begin{lstlisting}[language=sh]
make && time ./main
\end{lstlisting}
  \end{attention}

Vous pourrez utilisez le logiciel \sh|R| pour tracer les courbes. Une
possibilité est que votre programme écrive les mesures dans un
fichier, puis qu'il appelle \sh|R| pour visualiser ce fichier en
utilisant la fonction \|system|.

Faites les transformations mathématiques nécessaires pour mettre en
évidence l'ordre de grandeur de la complexité.

\subsection{Mesure d'espace \protect\starlevel{2}}
\label{sec:espace}

Vous pouvez instrumenter la classe \|Matrix| pour garder trace des
créations et destructions. Vous pouvez par exemple considérer en
première approximation que la taille allouée pour une matrice $(n,p)$
est $n \times p \times \text{\|sizeof(scalar_t)|}$. 
\begin{attention}
  Utilisez un attribut \|static| pour accumuler les tailles.
\end{attention}

Vous pouvez par exemple calculer la taille totale allouée et la taille
instantanée maximale occupée par votre programme. Comme ci-dessus,
faites très attention à ce que vous mesurez: faites une moyenne et un
écart-type sur une série d'exécution en oubliant les premières.

Tracez les courbes obtenues. Faites les transformations mathématiques
nécessaires pour mettre en évidence l'ordre de grandeur de la
complexité.

\subsection{Mesure d'espace au niveau système \protect\starlevel{3}}

\begin{attention}
  Ne choisissez cette option que si vous êtes déjà familier avec
  l'utilisation de \|malloc| en programmation système.
\end{attention}

Faites de même que pour la section~\ref{sec:espace}, mais cette
fois-ci en redéfinissant les opérateurs \|new| et \|new[ ]|. Vous
pourrez alors tracer précisément les allocations effectuées par votre
programme.

Pour une documentation complète, voir le site de référence. 
\begin{attention}
  \url{http://www.cplusplus.com/reference/new/}
\end{attention}
Regardez aussi la documentation associée sur le site.

\subsection{Optimisations logicielles \protect\starlevel{2}}

L'implémentation de la classe \|Matrix| proposée dans le squelette est
très inefficace. Beaucoup d'optimisations sont possibles. Choisissez
dans la liste ci-dessous... ou proposez mieux, ô futurs chercheurs!

\subsubsection*{Linéarisation}

L'utilisation de vecteurs de vecteurs est très pénalisante. Cependant,
il n'y a aucune raison de représenter une matrice ainsi. On peut
représenter une matrice $(n,p)$ comme un vecteur de taille
$n \times p$, étant entendu que l'élément $(i,j)$ se trouve par
exemple à l'indice $(i \times p) + j$. 
\begin{attention}
  Vérifiez que c'est bien une bijection! En fait, n'importe quelle
  bijection convient, par exemple $i + (j \times n)$. Faites des
  essais et mesurez l'impact.
\end{attention}

\subsubsection*{Accès non protégé}

Une fois le code au point, on peut utiliser des accès non protégés:
\|content[k]| au lieu de \|contents.at(k)|.

\subsubsection*{\emph{Inlining}}

Le langage C++ propose le qualificateur \|inline| pour indiquer au
compilateur que l'appel \emph{pourrait} être fait par une réécriture
de code au lieu d'un appel de fonction. Pour une documentation
complète, voir l'article \emph{What is C++ inline functions} du site
de référence.

\subsubsection*{À vous de jouer!}

Il y a beaucoup d'autres approches possibles. Je vous laisse le
plaisir de les découvrir... et de m'étonner!

\subsection*{Sous-matrices implicites \starlevel{3}}

Dans le squelette proposé, on construit explicitement les
sous-matrices, ce qui coûte très cher. En fait, c'est inutile. Il
suffit de les représenter par deux tableaux de bits, un pour les
lignes et un pour les colonnes. Chaque tableau indique les lignes et
les colonnes ``actives''. 

On peut donc définir une nouvelle implémentation de la classe
\|Matrix| qui gère le tableau des coefficients comme un attribut
\|static| et dont les deux attributs sont ces vecteurs de bits. Il
faut alors redéfinir les méthodes d'accès \|get| et \|set| et ajouter
une méthode d'extraction d'une sous-matrice. 

\begin{attention}
  Cette mise au point est délicate. Respectez une approche extrêmement
  défensive en utilisant abondamment les assertions.
\end{attention}

\subsection*{Méthode de Gauss-Jordan \protect\starlevel{3}}

Bien sûr, la meilleure optimisation est de travailler au niveau de la
méthode mathématique. La méthode de Cramer est élégante d'un point de
vue théorique mais inutilisable en pratique à cause de sa complexité
exponentielle. 

La méthode de Gauss-Jordan ne fournit pas de forme mathématique
explicite pour la solution, mais par contre elle construit la solution
de manière très efficace.

Sa stabilité numérique est dépendante du choix du pivot à chaque
phase. Soignez bien cet aspect.

Vous aurez besoin de permuter des lignes et des colonnes. Ajoutez ces
méthodes à votre classe.

Une approche élégante consiste à manipuler des matrices $(n, 2n)$ en
juxtaposant la matrice à inverser et la matrice identité.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
